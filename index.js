const fs = require('fs').promises;
const cheerio = require('cheerio')
const puppeteer = require('puppeteer');

async function loadPage() {
  const browser = await puppeteer.launch({headless: ! process.env.HEADFUL });
  const page = await browser.newPage();
  await page.goto("https://www.portes-ouvertes-epfl.ch/");
  await page.waitFor(5000); // Nobody is perfect
  let retval = await page.evaluate(() => document.body.innerHTML)
  await browser.close()
  return retval
}

async function main() {

  const html = await loadPage()
  const $ = cheerio.load(html)

  let result = $('.event-representation').map((i, e) => {
    let title = $('.event-title', e).html();
    let day = $('.time-data span', e).html();
    let time = $('.time-data span+span', e).html();
    let description = $('.d-md-block', e).html();
    let url = $('.image-event > a', e).attr('href');
    let img_url = $('.image-event > img+img', e).attr('src');
    let [location, type] = $('.event-location', e).html().split(' | ')

    return {url, title, description, location, day, time, type, img_url}
  }).toArray()

  await fs.writeFile("result.json",JSON.stringify(result))
  return result

}

main().then(console.log).catch(console.error)